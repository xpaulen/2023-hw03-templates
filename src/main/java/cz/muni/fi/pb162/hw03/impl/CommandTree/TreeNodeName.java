package cz.muni.fi.pb162.hw03.impl.CommandTree;

import cz.muni.fi.pb162.hw03.template.model.TemplateModel;

/**
 * tree node representing Name
 * @author Adam Paulen
 */
public class TreeNodeName implements TreeNode{

    private String name;
    private TreeNode next = null;

    /**
     * constructor for Name TreeNode
     *
     * @param name name?
     */
    public TreeNodeName(String name) {
        this.name = name;
    }

    @Override
    public String evaluate(TemplateModel model) {
        return model.getAsString(name);
    }

    @Override
    public TreeNode getNext() {
        return next;
    }

    @Override
    public void setNext(TreeNode node) {
        next = node;
    }
}
