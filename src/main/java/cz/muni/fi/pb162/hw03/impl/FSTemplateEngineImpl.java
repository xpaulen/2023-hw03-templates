package cz.muni.fi.pb162.hw03.impl;

import cz.muni.fi.pb162.hw03.template.FSTemplateEngine;
import cz.muni.fi.pb162.hw03.template.model.TemplateModel;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

/**
 * Implementation of FSTemplateEngine
 *
 * @author Adam Paulen
 */
public class FSTemplateEngineImpl extends TemplateEngineImpl implements FSTemplateEngine {
    @Override
    public void loadTemplate(Path file, Charset cs, String ext) {
        String filename = file.getFileName().toString();
        String templateName = filename.substring(0, filename.lastIndexOf(ext) - 1);
        try {
            List<String> lines = Files.readAllLines(file, cs);
            loadTemplate(templateName, String.join(System.lineSeparator(), lines));
        } catch (IOException ignored) {
        }
    }

    @Override
    public void loadTemplateDir(Path inDir, Charset cs, String ext) {
        try (Stream<Path> paths = Files.walk(inDir)) {
            paths.filter(Files::isRegularFile)
                    .forEach(c -> loadTemplate(c, cs, ext));
        } catch (IOException ignored) {
        }
    }

    @Override
    public void writeTemplate(String name, TemplateModel model, Path file, Charset cs) {
        try {
            Files.writeString(file, evaluateTemplate(name, model), cs);
        } catch (IOException ignored) {
        }
    }

    @Override
    public void writeTemplates(TemplateModel model, Path outDir, Charset cs) {
        for (String name : getTemplateNames()) {
            try {
                Path file = outDir.resolve(name);
                Files.writeString(file, evaluateTemplate(name, model), cs);
                Files.createFile(file);
            } catch (IOException ignored) {
            }
        }
    }
}
