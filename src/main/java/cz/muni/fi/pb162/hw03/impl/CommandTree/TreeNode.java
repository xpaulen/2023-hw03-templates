package cz.muni.fi.pb162.hw03.impl.CommandTree;

import cz.muni.fi.pb162.hw03.template.model.TemplateModel;

/**
 * @author Adam Paulen
 */
public interface TreeNode {

    /**
     * evaluates current node using model
     *
     * @param model model to be used for evaluating
     * @return String of evaluation
     */
    String evaluate(TemplateModel model);

    /**
     * gets next node in the tree
     *
     * @return next node
     */
    TreeNode getNext();

    /**
     * Sets next node in tree
     *
     * @param node node to be set as next
     */
    void setNext(TreeNode node);
}
