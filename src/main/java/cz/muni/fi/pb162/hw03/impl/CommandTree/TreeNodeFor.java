package cz.muni.fi.pb162.hw03.impl.CommandTree;

import cz.muni.fi.pb162.hw03.template.model.TemplateModel;

/**
 * @author Adam Paulen
 */
public class TreeNodeFor implements TreeNode{

    private String name;
    private String in;
    private TreeNode cycle;
    private TreeNode next = null;

    /**
     * constructor for treeNode representing for cycle
     *
     * @param name name to be used
     * @param in name of iterable
     * @param cycle nodes to cycle through
     */
    public TreeNodeFor(String name, String in, TreeNode cycle) {
        this.name = name;
        this.in = in;
        this.cycle = cycle;
    }

    @Override
    public String evaluate(TemplateModel model) {
        StringBuilder output = new StringBuilder();
        for(Object o : model.getAsIterable(in)){
            TemplateModel model1 = model.copy().put(name, o);
            TreeNode node = cycle;
            while(node != null){
                output.append(node.evaluate(model1));
                node = node.getNext();
            }
        }
        return output.toString();
    }

    @Override
    public TreeNode getNext() {
        return next;
    }

    @Override
    public void setNext(TreeNode node) {
        next = node;
    }

    public void setCycle(TreeNode node){
        cycle = node;
    }
}
