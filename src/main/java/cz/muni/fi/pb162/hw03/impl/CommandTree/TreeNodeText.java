package cz.muni.fi.pb162.hw03.impl.CommandTree;

import cz.muni.fi.pb162.hw03.template.model.TemplateModel;

/**
 * Tree Node representing Text
 *
 * @author Adam Paulen
 */
public class TreeNodeText implements TreeNode{

    private String text;

    private TreeNode next = null;

    /**
     * constructor for Text TreeNode
     *
     * @param text text
     */
    public TreeNodeText(String text) {
        this.text = text;
    }

    @Override
    public String evaluate(TemplateModel model) {
        return text;
    }

    @Override
    public TreeNode getNext() {
        return next;
    }

    @Override
    public void setNext(TreeNode node) {
        next = node;
    }
}
