package cz.muni.fi.pb162.hw03.impl.CommandTree;

import cz.muni.fi.pb162.hw03.template.model.TemplateModel;

/**
 * TreeNode representing If statement
 * @author Adam Paulen
 */
public class TreeNodeIf implements TreeNode {

    private String name;
    private TreeNode next = null;
    private TreeNode treeTrue;
    private TreeNode treeFalse;

    /**
     * constructor for If TreeNode
     *
     * @param name name of bool parameter
     * @param treeTrue nodes to write if parameter is true
     * @param treeFalse nodes to write if parameter is false
     */
    public TreeNodeIf(String name, TreeNode treeTrue, TreeNode treeFalse) {
        this.name = name;
        this.treeTrue = treeTrue;
        this.treeFalse = treeFalse;
    }

    @Override
    public String evaluate(TemplateModel model) {
        StringBuilder output = new StringBuilder();
        TreeNode node = model.getAsBoolean(name) ? treeTrue : treeFalse;
        while(node != null){
            output.append(node.evaluate(model));
            node = node.getNext();
        }
        return output.toString();
    }

    @Override
    public TreeNode getNext() {
        return next;
    }

    @Override
    public void setNext(TreeNode node) {
        next = node;
    }

    public void setTrue(TreeNode node){
        treeTrue = node;
    }

    public void setFalse(TreeNode node){
        treeFalse = node;
    }
}
