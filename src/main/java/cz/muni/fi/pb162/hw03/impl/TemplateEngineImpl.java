package cz.muni.fi.pb162.hw03.impl;

import cz.muni.fi.pb162.hw03.impl.CommandTree.TreeNode;
import cz.muni.fi.pb162.hw03.impl.CommandTree.TreeNodeFor;
import cz.muni.fi.pb162.hw03.impl.CommandTree.TreeNodeIf;
import cz.muni.fi.pb162.hw03.impl.CommandTree.TreeNodeName;
import cz.muni.fi.pb162.hw03.impl.CommandTree.TreeNodeText;
import cz.muni.fi.pb162.hw03.impl.parser.tokens.Token;
import cz.muni.fi.pb162.hw03.impl.parser.tokens.Tokenizer;
import cz.muni.fi.pb162.hw03.template.TemplateEngine;
import cz.muni.fi.pb162.hw03.template.TemplateException;
import cz.muni.fi.pb162.hw03.template.model.TemplateModel;
import cz.muni.fi.pb162.hw03.impl.parser.tokens.Commands;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * implementation of TemplateEngine
 *
 * @author Adam Paulen
 */
public class TemplateEngineImpl implements TemplateEngine {

    private final Map<String, TreeNode> templateMap = new HashMap<>();

    @Override
    public void loadTemplate(String name, String text) {
        Tokenizer tokenizer = new Tokenizer(text);
        TreeNode head = new TreeNodeText("");
        TreeNode curr = head;
        while (!tokenizer.done()) {
            Token token = tokenizer.consume();
            switch (token.getKind()) {
                case TEXT -> curr.setNext(new TreeNodeText(token.text()));
                case OPEN -> parseOpen(tokenizer, curr);
                default -> throw new TemplateException("wrong format");
            }
            while (curr.getNext() != null) {
                curr = curr.getNext();
            }
        }
        templateMap.put(name, head);
    }

    private String parseOpen(Tokenizer tok, TreeNode curr) {
        Token token = tok.consume();
        switch (token.getKind()) {
            case NAME -> {
                curr.setNext(new TreeNodeName(token.name()));
                if (tok.consume().getKind() != Token.Kind.CLOSE) {
                    throw new TemplateException("close brackets were not found");
                }
                return "name";
            }
            case CMD -> {
                curr.setNext(parseCMD(tok, token.cmd()));
                return token.cmd();
            }
            default -> throw new TemplateException("wrong format");
        }
    }

    private TreeNode parseCMD(Tokenizer tok, String cmd) {
        switch (cmd) {
            case Commands.IF -> {
                return parseIf(tok);
            }
            case Commands.FOR -> {
                return parseFor(tok);
            }
            case Commands.DONE, Commands.ELSE -> {
                if(tok.consume().getKind() != Token.Kind.CLOSE){
                    throw new TemplateException("close brackets not found");
                }
                return null;
            }
            default -> throw new TemplateException("wrong format");
        }
    }

    private TreeNode parseIf(Tokenizer tok) {
        String name = tok.consume().name();
        if (tok.consume().getKind() != Token.Kind.CLOSE) {
            throw new TemplateException("close brackets were not found");
        }
        TreeNode treeTrue = new TreeNodeText("");
        TreeNode treeFalse = new TreeNodeText("");
        TreeNode curr = treeTrue;
        TreeNodeIf treeIf = new TreeNodeIf(name, treeTrue, treeFalse);
        boolean done = false;
        while (!done) {
            Token token = tok.consume();
            switch (token.getKind()) {
                case TEXT -> curr.setNext(new TreeNodeText(token.text()));
                case OPEN -> {
                    String commandName = parseOpen(tok, curr);
                    if (commandName.equals(Commands.ELSE)) {
                        curr = treeFalse;
                    } else if (commandName.equals(Commands.DONE)) {
                        done = true;
                    }
                }
                default -> throw new TemplateException("");
            }
            while (curr.getNext() != null) {
                curr = curr.getNext();
            }
            if (tok.done() && !done) {
                throw new TemplateException("command done was not provided");
            }
        }
        return treeIf;
    }

    private TreeNode parseFor(Tokenizer tok) {
        String name = tok.consume().name();
        if (tok.consume().getKind() != Token.Kind.IN) {
            throw new TemplateException("missing : symbol");
        }
        String in = tok.consume().name();
        if (tok.consume().getKind() != Token.Kind.CLOSE) {
            throw new TemplateException("missing closing brackets");
        }
        TreeNode cycle = new TreeNodeText("");
        TreeNode curr = cycle;
        TreeNodeFor treeFor = new TreeNodeFor(name, in, cycle);
        boolean done = false;
        while (!done) {
            Token token = tok.consume();
            switch (token.getKind()) {
                case TEXT -> curr.setNext(new TreeNodeText(token.text()));
                case OPEN -> {
                    String commandName = parseOpen(tok, curr);
                    if (commandName.equals(Commands.ELSE)) {
                        throw new TemplateException("else statement in for command");
                    } else if (commandName.equals(Commands.DONE)) {
                        done = true;
                    }
                }
                default -> throw new TemplateException("invalid format");
            }
            while (curr.getNext() != null) {
                curr = curr.getNext();
            }
            if (tok.done() && !done) {
                throw new TemplateException("command done was not provided");
            }
        }
        return treeFor;
    }

    @Override
    public Collection<String> getTemplateNames() {
        return templateMap.keySet();
    }

    @Override
    public String evaluateTemplate(String name, TemplateModel model) {
        if (!templateMap.containsKey(name)) {
            throw new TemplateException("name not found");
        }
        TreeNode node = templateMap.get(name);
        StringBuilder output = new StringBuilder();
        while (node != null) {
            output.append(node.evaluate(model));
            node = node.getNext();
        }
        return output.toString();
    }
}
